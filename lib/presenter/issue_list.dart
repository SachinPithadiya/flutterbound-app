import 'package:flut_bount/view/issueController.dart';
import 'package:flutter/material.dart';

class IssueList extends StatefulWidget {
  @override
  _IssueListState createState() => _IssueListState();
}

class _IssueListState extends State<IssueList> {
  int _cIndex = 0;
  var screens = [
    IssueLists(),
    MyProfile(),
    AddIssue(),
    SolvedIssue(),
    FunddedIssue()
  ];
  void _changeTab(index) {
    setState(() {
      _cIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flut Bount"),
      ),
      body: screens[_cIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _cIndex,
        type: BottomNavigationBarType.shifting,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            title: Text('Github Issue List'),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle), title: Text('My Profile')),
          BottomNavigationBarItem(
              icon: Icon(Icons.add), title: Text('Add Issue')),
          BottomNavigationBarItem(
              icon: Icon(Icons.check_circle_outline),
              title: Text('Solved Issue')),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_balance_wallet),
              title: Text('Fundded Issue'))
        ],
        onTap: (index) {
          _changeTab(index);
        },
      ),
    );
  }
}

class IssueLists extends StatefulWidget {
  @override
  _IssueListsState createState() => _IssueListsState();
}

class _IssueListsState extends State<IssueLists> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: <Widget>[
          issueList,
          issueList,
          issueList,
          issueList,
          issueList,
          issueList,
          issueList,
          issueList,
          issueList,
          issueList,
          issueList,
          issueList,
        ],
      ),
    );
  }
}

class AddIssue extends StatefulWidget {
  @override
  _AddIssueState createState() => _AddIssueState();
}

class _AddIssueState extends State<AddIssue> {
  @override
  Widget build(BuildContext context) {
    return BountyCreate();
  }
}

class MyProfile extends StatefulWidget {
  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  @override
  Widget build(BuildContext context) {
    return MyHomePage();
  }
}

class SolvedIssue extends StatefulWidget {
  @override
  _SolvedIssueState createState() => _SolvedIssueState();
}

class _SolvedIssueState extends State<SolvedIssue> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class FunddedIssue extends StatefulWidget {
  @override
  _FunddedIssueState createState() => _FunddedIssueState();
}

class _FunddedIssueState extends State<FunddedIssue> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class BountyCreate extends StatefulWidget {
  @override
  _BountyCreateState createState() => _BountyCreateState();
}

class _BountyCreateState extends State<BountyCreate> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                OutlineButton(
                  child: Text('Public'),
                  onPressed: () {},
                ),
                SizedBox(width: 50),
                OutlineButton(
                  child: Text('Private'),
                  onPressed: () {},
                ),
              ],
            ),
            SizedBox(
              width: 100,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 100),
                child: TextField(
                  decoration: InputDecoration(hintText: "Enter URL of repo"),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                OutlineButton(
                  child: Text('Sync with Github'),
                  onPressed: () {},
                ),
              ],
            ),
            SizedBox(
              width: 100,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.teal),
                    ),
                    hintText: 'Issue Title',
                    suffixStyle: const TextStyle(color: Colors.green),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              width: 100,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.teal),
                    ),
                    hintText: 'Issue Description',
                    suffixStyle: const TextStyle(color: Colors.green),
                  ),
                  maxLines: 4,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10),
                  TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.teal),
                      ),
                      hintText: 'USD',
                      suffixStyle: const TextStyle(color: Colors.green),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.teal),
                      ),
                      hintText: 'Est. hours of work',
                      suffixStyle: const TextStyle(color: Colors.green),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                OutlineButton(
                  child: Text('Submit'),
                  onPressed: () {},
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final String _fullName = "jay vekariya";
  final String _status = "Flutter Developer";
  final String _bio =
      "\"Hi, I am a Flutter developer working for hourly basis. If you wants to contact me to build your product leave a message.\"";
  final String _issue = "50";
  final String _resolve = "20";
  final String _paid = "20";

  Widget _buildCoverImage(Size screenSize) {
    return Container(
      height: screenSize.height / 2.6,
      decoration: BoxDecoration(
        color: Colors.black,
      ),
    );
  }

  Widget _buildProfileImage() {
    return Center(
      child: Container(
        width: 140.0,
        height: 140.0,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage("https://via.placeholder.com/100"),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(80.0),
          border: Border.all(
            color: Colors.white,
            width: 10.0,
          ),
        ),
      ),
    );
  }

  Widget _buildFullName() {
    TextStyle _nameTextStyle = TextStyle(
      color: Colors.black,
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
    );

    return Text(
      _fullName,
      style: _nameTextStyle,
    );
  }

  Widget _buildStatus(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Text(
        _status,
        style: TextStyle(
          fontFamily: 'Spectral',
          color: Colors.black,
          fontSize: 20.0,
          fontWeight: FontWeight.w300,
        ),
      ),
    );
  }

  Widget _buildStatItem(String label, String count) {
    TextStyle _statLabelTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 16.0,
      fontWeight: FontWeight.w200,
    );

    TextStyle _statCountTextStyle = TextStyle(
      color: Colors.black54,
      fontSize: 24.0,
      fontWeight: FontWeight.bold,
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          count,
          style: _statCountTextStyle,
        ),
        Text(
          label,
          style: _statLabelTextStyle,
        ),
      ],
    );
  }

  Widget _buildStatContainer() {
    return Container(
      height: 60.0,
      margin: EdgeInsets.only(top: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildStatItem("Issue", _issue),
          _buildStatItem("Resolve", _resolve),
          _buildStatItem("Paid", _paid),
        ],
      ),
    );
  }

  Widget _buildBio(BuildContext context) {
    TextStyle bioTextStyle = TextStyle(
      fontFamily: 'Spectral',
      fontWeight: FontWeight.w400,
      fontStyle: FontStyle.italic,
      color: Color(0xFF799497),
      fontSize: 16.0,
    );

    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.all(20.0),
      child: Text(
        _bio,
        textAlign: TextAlign.center,
        style: bioTextStyle,
      ),
    );
  }

  Widget _gitlink() {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage("https://via.placeholder.com/100"),
                  fit: BoxFit.fill,
                ),
              ),
              child: InkWell(
                onTap: () {
                  print("git link open");
                },
              ),
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        _buildCoverImage(screenSize),
        SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(height: screenSize.height / 4.4),
                _buildProfileImage(),
                _buildFullName(),
                _buildStatus(context),
                _buildStatContainer(),
                _buildBio(context),
                _gitlink(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
