class issueModel {
  final int issueId;
  final String issueTitle;
  final String issueDescription;
  final String fund;

  issueModel({this.issueId, this.issueTitle, this.issueDescription, this.fund});
  factory issueModel.fromJson(Map<String, dynamic> json) {
    return issueModel(
        issueId: json['issueid'],
        issueTitle: json['issueTitle'],
        issueDescription: json['issueDescription'],
        fund: json["fund"]);
  }
}
