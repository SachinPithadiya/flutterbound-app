import 'package:flut_bount/model/issueModel.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<issueModel> fetchIssue() async {
  final response =
      await http.get('https://jsonplaceholder.typicode.com/posts/1');
  if (response.statusCode == 200) {
    return issueModel.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to load issue');
  }
}

Widget issueList = Container(
  height: 200,
  width: double.infinity,
  padding: EdgeInsets.only(left: 10, right: 10, top: 20),
  child: Card(
    color: Colors.white,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Research Grant: Swarm Scraper For Verified Smart Contacts",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Fund \$5000",
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        SizedBox(
          width: 100,
          child: RaisedButton(
            onPressed: () {},
            child: Center(
              child: Text("Start Work"),
            ),
            color: Colors.purple,
          ),
        )
      ],
    ),
  ),
);
