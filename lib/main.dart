import 'package:flutter/material.dart';
import 'package:flut_bount/presenter/issue_list.dart';

void main() => runApp(FlutBount());

class FlutBount extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flut Bound',
      theme: ThemeData(
        textTheme: TextTheme(
            headline: TextStyle(
                fontSize: 27,
                fontWeight: FontWeight.bold,
                color: Colors.greenAccent),
            title: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
            body1: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.normal,
                color: Colors.white)),
        brightness: Brightness.dark,
        primaryColor: Colors.black,
        accentColor: Colors.white,
      ),
      home: IssueList(),
    );
  }
}
